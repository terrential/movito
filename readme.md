#Movito - A self-hosted YouTube alternative. 

Delivered in a docker image for maximum portability.

More specifically, it's a docker image that is based off the official wordpress image, but with FFMPEG installed, and a custom child theme (based off HappyTheme's VideoHost theme) and a set of plugins.

#Why Do I Need This?

You've got videos to show, but you don't want to show them to everybody, and you definitely don't want Google all over your content!  Plus there might be corporate restrictions on using software/applications that are external to the network.  This gets round that offering the ability to upload and manage video content within the network, using open source tools.

Using other tools such as MediaCore (MediaDrop), etc. were often very tricky to use and get setup and quite often out of date.  By using WordPress as an application framework, we can deliver a much more value to the end users faster than developing something from scratch.

#Before You Start Start

There are a few things that are required before you start:

###A MySql/MariaDB instance###
You'll need the name of a database that you have created earlier, along with access credentials, just the same type as you'd use with a regular wordpress install.  I've found that creating another linked container for mysql is an easy way to manage this inside the network rather than having to get access to another mysql server.  I've found that the bitnami/mariadb image is an excellent choice for this.

###A Volume Mount For Your Uploaded Content###
It would be handy to mount a volume for the /var/www/html/wp-content/uploads folder so that any content that is uploaded is persisted after the container has been removed, this keeps the content external to the container image. It's likely to get very big very quickly.

#Getting It Running

1. Start the container: `docker run --name <containername> --link <linked-mysql-container-name>:mysql -p <desiredport>:80 -d movito`

2. At this stage, you'll be shown the basic WordPress installation screen, follow the prompts, and get logged into the dashboard.

3. Switch to the Movito theme, it will alert you which plugins need to be installed and setup the required site options (roles, etc) for you.

#Configuration

Once you are logged in, the only thing to do decide on how your users are going to get logged in.  You could allow users to register themselves with their own login details via the built in registration system (manual. for smaller companies), or you could allow them to login via an Office 365 account (automatic, for larger companies). The wpo365-login takes care of this for you. Follow the configuration guide, it makes getting started really easy.

[wpo365-login configuration guide](https://www.wpo365.com/how-to-install-wordpress-office-365-login-plugin/)

If your setup works, when an anonymous user visits the site, they'll be redirected to office365 to login, then back to the website (based on the redirect url you setup in the azure portal - see the guide), but this time, they'll have a user account created automatically for them and they'll be logged into it.  This user should have the subscriber role.

#Uploading Videos

Each video that a user uploads, requires creating a new post for each one (well, you can actually put more than one video in each post, but I wouldn't recommend it). Once you've added the title, click the Add Media button, and upload a video (preferably .mp4, though others might work).  Using the Video Embed & Thumbnail Generator plugin modifies the video upload screen so you can select a frame of the video to use or upload a custom 'poster' to use instead.  When you are done, click on the blue Insert button to add the shortcode to the post content area. Hit the publish button.

[Video Embed Thumbnail Generator Wordpress Plugin](https://www.kylegilman.net/2011/01/18/video-embed-thumbnail-generator-wordpress-plugin/)


#Playback of the Video Content

When you view the video at the front end of the website, it's uses the built in WordPress media player, and if you use the default media player and the mp4 file format, WordPress will offer the ability to download the video in a `chunked` format, so you effectively have a streaming service built in.  This way the bandwidth isn't being sucked up by buffering all the video that is being offered.